﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrRandomWalk : MonoBehaviour
{
    public float speed = 10;
    public Color color;
    public float eatenSurvivorChance = (float) 0.7;
    //public float eatenCloneChance = (float) 0.6;
    public float starvedSurvivorChance = (float)0.3;
    //public float starvedCloneChance = (float)0.1;
    bool eaten = false;

    private void Start()
    {
        gameObject.GetComponent<Renderer>().material.color = color;

    }

    void OnTriggerEnter(Collider other)
    {
        eaten = true;
        Destroy(other.gameObject);
    }

    public IEnumerator RunSimulation()
    {
        eaten = false;
        float n = Random.Range(-speed/2, speed/2);
        float n1 = Random.Range(-speed/2, speed/2);
        for (int i = 0; i < 24*5; i++)
        {
            n += Random.Range(-speed, speed);
            n1 += Random.Range(-speed, speed);
            if (n > speed || n < -speed)
            {
                n = n / 2;
            }
            if (n1 > speed || n < -speed)
            {
                n1 = n1 / 2;
            }
            Debug.Log(new Vector3(n, 0, n1));
            transform.position = transform.position + new Vector3(n, 0, n1);
            if (transform.position.x > 15 || transform.position.x < -15)
            {

                transform.position = transform.position - new Vector3(n, 0, n1);
                n *= -1;
            }
            if (transform.position.z < -15 || transform.position.z > 15)
            {

                transform.position = transform.position - new Vector3(n, 0, n1);
                n1 *= -1;
            }
            yield return new WaitForSeconds(1/24f);
        }
        float dado = Random.Range(0f, 1f);
        if (eaten)
        {
            Debug.Log(dado);
            if (dado > eatenSurvivorChance)
            {
                Debug.Log("Muere");
                Destroy(gameObject);
            } else
            {
                Debug.Log("Vive");

            }
        }
        else
        {
            Debug.Log(dado);
            if (dado > starvedSurvivorChance)
            {
                Debug.Log("Muere");
                Destroy(gameObject);
            }
            else
            {
                Debug.Log("Vive");
                
            }
        }
    }
    public void Cycle()
    {
        StartCoroutine("RunSimulation");
    }
}