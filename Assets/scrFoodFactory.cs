﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrFoodFactory : MonoBehaviour
{
    public GameObject food;
    public int amount;
    // Start is called before the first frame update
    public void Generar()
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject aux = Instantiate(food);
            aux.transform.position = new Vector3(Random.Range(-15f, 15f),0, (Random.Range(-15f, 15f)));
        }
    }


}
