﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrRandomWalkProtFly : MonoBehaviour
{
    public GameObject Prototype;
    public string prototypeName;
    public GameObject CreatureFlyWeight;
    bool eaten = false;

    private void Awake()
    {
        gameObject.GetComponent<Renderer>().material.color = CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().color;
        Prototype = GameObject.Find(prototypeName);
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Food")
        {
            eaten = true;
            Destroy(other.gameObject);
        }

    }

    public IEnumerator RunSimulation()
    {
        eaten = false;
        float n = 0;
        float n1 = 0;
        for (int i = 0; i < 24 * 5; i++)
        {
            n += Random.Range(-CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().speed, CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().speed);
            n1 += Random.Range(-CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().speed, CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().speed);
            if (n > CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().speed || n < -CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().speed)
            {
                n = n / 2;
            }
            if (n1 > CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().speed || n < -CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().speed)
            {
                n1 = n1 / 2;
            }
            Debug.Log(new Vector3(n, 0, n1));
            transform.position = transform.position + new Vector3(n, 0, n1);
            if (transform.position.x > 15 || transform.position.x < -15)
            {

                transform.position = transform.position - new Vector3(n, 0, n1);
                n *= -1;
            }
            if (transform.position.z < -15 || transform.position.z > 15)
            {

                transform.position = transform.position - new Vector3(n, 0, n1);
                n1 *= -1;
            }
            yield return new WaitForSeconds(1 / 24f);
        }
        float dado = Random.Range(0f, 1f);
        if (eaten)
        {
         
            if (dado > CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().eatenSurvivorChance)
            {
                Debug.Log("Muere");
                Prototype.GetComponent<scrPrototypeFly>().Eliminate(gameObject);
            }
            else
            {
                Debug.Log("Vive");
                dado = Random.Range(0f, 1f);
                if (dado < CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().eatenCloneChance)
                {
                    Debug.Log("Se Clona");
                    Prototype.GetComponent<scrPrototypeFly>().Clone(gameObject);
                }
            }
        }
        else
        {
            Debug.Log(dado);
            if (dado > CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().starvedSurvivorChance)
            {
                Debug.Log("Muere");
                Prototype.GetComponent<scrPrototypeFly>().Eliminate(gameObject);

            }
            else
            {
                Debug.Log("Vive");
                dado = Random.Range(0f, 1f);
                if (dado < CreatureFlyWeight.GetComponent<scrCreatureFlyWeight>().starvedCloneChance)
                {
                    Debug.Log("Se Clona");
                    Prototype.GetComponent<scrPrototypeFly>().Clone(gameObject);
                }
            }
        }
    }
    public void Cycle()
    {
        StartCoroutine("RunSimulation");
    }
}
