﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrPrototypeFly : MonoBehaviour
{
    public GameObject CreaturaPrototipo;
    public int poblacionInicial;
    List<GameObject> listCreatures = new List<GameObject>();
    // Update is called once per frame
    public void Cycle()
    {
        foreach (GameObject creatura in listCreatures)
        {
            creatura.GetComponent<scrRandomWalkProtFly>().Cycle();
        }
    }

    private void Awake()
    {
        for (int i = 0; i < poblacionInicial; i++)
        {
            GameObject aux = Instantiate(CreaturaPrototipo);
            aux.transform.position = new Vector3(Random.Range(-15f, 15f), 0, (Random.Range(-15f, 15f)));
            listCreatures.Add(aux);

        }
    }

    public void Clone(GameObject clone)
    {
        GameObject aux = Instantiate(clone);
        listCreatures.Add(aux);
    }

    public void Eliminate(GameObject clone)
    {
        listCreatures.Remove(clone);
        Destroy(clone);
    }
}
