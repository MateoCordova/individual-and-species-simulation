﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrCreatureFlyWeight : MonoBehaviour
{
    public float speed = 10;
    public Color color;
    public float eatenSurvivorChance = (float)0.7;
    public float eatenCloneChance = (float)0.6;
    public float starvedSurvivorChance = (float)0.3;
    public float starvedCloneChance = (float)0.1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
